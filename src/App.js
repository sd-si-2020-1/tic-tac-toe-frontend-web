import React from 'react';
import Board from './Board';

function App() {
  return (
    <Board
      base_url='http://localhost:8080/'
      board_route='tictactoe/board'
      move_route='tictactoe/move/'
      status_route='tictactoe/status/all'  
    />  
  );
}

export default App;
