import React, { useEffect, useRef } from 'react'
import './Game.css'

export function Square(props) { 
  
  const canvasRef = useRef()
  
  /* const handleClick = () => {
    alert('click from Square of id: '+props.id)
  }*/

  useEffect(() => {
    const ctx = canvasRef.current.getContext('2d')
    ctx.font = '25px arial'
    if ( props.value === 'X' ) {
      ctx.fillText('X', 15, 30)
    }
    else if ( props.value === 'O' ) {
      ctx.fillText('O', 15, 30)
    }  
  }, [props.value])

  return(
    <canvas
      // onClick={handleClick}
      onClick={()=>{props.onClick(props.id)}}
      className="square"
      ref={canvasRef}
      height={props.height}
      width={props.width}
      style={{backgroundColor: '#f1f1f1'}}
    />
  )
}

export default Square
