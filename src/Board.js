import React, {useState, useEffect} from 'react';
import axios from 'axios';
import { Square } from './Square'
import './Game.css'

export function Board(props) {
  const { 
    base_url, 
    move_route,
    board_route,
    status_route} = props;
  const initial_squares = 
    props.squares ? props.squares : 
    [null,null,null, null, null, null,null,null,null];
  const initial_status = {
      status: null,
      winner: null,
      next: 'X'
  };
  const [status, setStatus] = useState(initial_status);
  const [squares,setSquares] = useState(initial_squares);
  const [header, setHeader] = useState('');
  const [footer, setFooter] = useState('');
  const handleClick = (id) => {
    axios.get(base_url+move_route+id)
      .then((result)=> setSquares(result.data))
      .then(
        axios.get(base_url+status_route)
        .then((result)=>setStatus(result.data))
      )
      .catch((error)=>{
        console.log(JSON.stringify(error))
      })
  }
  useEffect(()=>{
    console.log('status: '+JSON.stringify(status))
    if (status.status) {
      setHeader('Game: '+status.status)
      if (status.status == 'opened')
        setFooter('Next player: '+status.next)
      else if (status.status == 'closed') {
        if (status.winner == null)
          setFooter('Result: game tied')
        else
          setFooter('Result: '+status.winner+' won')  
      }
    }
    else
      setHeader('Game: not started')
  },[status]);
  useEffect(()=>{
    if (props.squares) return; // exit if props.squares is not null
    let req1 = axios.get(base_url+board_route);      
    let req2 = axios.get(base_url+status_route);
    Promise.all([req1, req2])    
      .then((results)=>{
          setSquares(results[0].data)
          setStatus(results[1].data)
      })
      .catch((error)=>console.log(error))
  },[])

  const renderSquare = (i) => <Square 
        // value={props.squares[i]}
        onClick={handleClick}
        value={squares[i]}
        width='50'
        height='50'
        id={i}
      />;
  return(
    <div>
        <div>{header}</div>
        <div className="board-row">
          {renderSquare(0)}
          {renderSquare(1)}
          {renderSquare(2)}
        </div>
        <div className="board-row">
          {renderSquare(3)}
          {renderSquare(4)}
          {renderSquare(5)}
        </div>
        <div className="board-row">
          {renderSquare(6)}
          {renderSquare(7)}
          {renderSquare(8)}
        </div>      
        <div>{footer}</div>
    </div>
  )
}

export default Board;