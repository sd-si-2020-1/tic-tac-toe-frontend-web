import React from 'react';
import { Square } from '../Square';

export default {
  title: 'Components/Square',
  component: Square,
}

export const Primary = () => <Square width='50' height='50' value='O'/>;
