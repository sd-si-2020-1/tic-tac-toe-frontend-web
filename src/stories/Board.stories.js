import React from 'react';
import { Board } from '../Board';

export default {
  title: 'Components/Board',
  component: Board,
}

export const Primary = () => 
  <Board       
    squares={[
      'X', null, 'O', 
      'O', 'X', null, 
      null, null, 'X'
    ]} 
/>;

// export const WithAxios = () => <Board />;
export const WithAxios = () => 
  <Board
    base_url='http://localhost:8080/'
    board_route='tictactoe/board'
    move_route='tictactoe/move/'
    status_route='tictactoe/status/all'  
/>;